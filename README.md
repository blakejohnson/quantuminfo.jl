# QuantumInfo.jl

This package contains functions that are convenient for common
calculation in quantum information processing. 

Install it with the following command:

	  Pkg.add("QuantumInfo")

# Acknowledgements

This research was funded by the Intelligence Advanced Research
Projects Activity (IARPA) Multi Qubit Coherent Operations (MQCO)
program under Contract No. W911NF-10-1-0324. All statements of fact,
opinion, or conclusions contained herein are those of the authors and
should not be construed as representing the official views or policies
of IARPA, ODNI, or the US Government.

# License

MIT Expat license.

# Authors and Copyright

Written by Marcus P da Silva (@marcusps). Contributions by Colm Ryan.

(c) 2014 Raytheon BBN Technologies



