VERSION >= v"0.4.0-dev+6521" && __precompile__()
module QuantumInfo

  include("basics.jl")
  include("open-systems.jl")

end
